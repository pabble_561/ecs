var browserify = require("browserify");
var tsify = require("tsify");
var source = require("vinyl-source-stream");
var buffer = require('vinyl-buffer');
var gulp = require("gulp");
var uglify = require("gulp-uglify")
//工作空间
var workSpaceDir = "";

//使用browserify，转换ts到js，并输出到bin/js目录
browserify({
    basedir: workSpaceDir,
    //是否开启调试，开启后会生成jsmap，方便调试ts源码，但会影响编译速度
    debug: false,
    entries: ['src/export.ts'],
    cache: {},
    module:"es2015",

    packageCache: {}
})
    //使用tsify插件编译ts
    .plugin(tsify)
    .bundle()
    //使用source把输出文件命名为bundle.js
    .pipe(source('core.js'))
    .pipe(buffer())
    .pipe(uglify())
    //把bundle.js复制到bin/js目录
    .pipe(gulp.dest(workSpaceDir + "dist/"))

