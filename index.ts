import {Engine} from "./src/Engine";
import {Actor} from "./src/Actor";
import {Vec3} from "./src/Vec3";
import {Move} from "./src/Move";
import {Decimal} from "decimal.js";
// import Decimal = require('decimal.js');



function main(){
    var scene = new Engine()
    scene.initSeed("hello")
    var role = new Actor("role")
    scene.addChild(role)

    role.point = Vec3.from(0,1,0)
    var move = role.addComponent(Move) as Move
    console.log("move:",move)

    setInterval(()=>{
        move.onFrame(0,null)
        console.log("x:",move.actor.point.x+"")
    },100)

    setTimeout(()=>{
        console.log("out")
        move.moveLinear.x = 0.1
    },1000)

    setTimeout(()=>{
        console.log("out")
        move.moveLinear.x = -0.1
    },2000)

    setTimeout(()=>{
        console.log("out")
        move.moveLinear.x = 0
    },3000)
}

main()