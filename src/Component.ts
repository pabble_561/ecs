import {Vec3} from "./Vec3";
import {LifeCycle} from "./Interface";
import {Actor} from "./Actor";

export class Component implements LifeCycle{
    _name:string
    _uuid:string

    actor : Actor = null


    destroy(){
        this.actor.destroyComponent(this)
    }

    Awake() {
    }

    OnEnable() {
    }
    OnDisabled() {
    }
    OnDestroy() {
    }
}