import {Component} from "./Component";
import {Vec3} from "./Vec3";
import {Frame} from "./Interface";

export class Move extends Component implements Frame{

    moveLinear : Vec3 = Vec3.ZERO
    angularvelocity : Vec3 = Vec3.ZERO

    setDirection(dir:Vec3){
        this.moveLinear = dir
    }
    setAngular(anr:Vec3){
        this.angularvelocity = anr
    }

    onFrame(frame:number,data:Uint8Array){
        if(this.moveLinear.x != 0 || this.moveLinear.y != 0 || this.moveLinear.y != 0){
            this.actor.point = this.actor.point.add(this.moveLinear)
        }
        if(this.angularvelocity.x != 0 || this.angularvelocity.y != 0 || this.angularvelocity.y != 0){
            this.actor.rotate = this.actor.rotate.add(this.angularvelocity)
        }
    }
}