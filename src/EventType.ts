export module EventType {
    export enum Actor {
        POSITION_CHANGE ="position_change",
        ANGULAR_CHANGE ="angular_change",
        ON_ENABLED = "enabled",
        OFF_ENABLED = "offabled",
        DESTROYED = "destroyed"
    }
}