export default class Event {
    private events : Array<any> = [];

    on(eventName, fun, target) {
        this.events.push({ eventName: eventName, fun: fun, target: target });
    }
    off(eventName, fun?, target?) {
        if(typeof eventName == 'object' || typeof eventName == 'function'){
            return this.removeAllListeners(eventName);
        }
        this.events = this.events.filter(item => { return item.eventName != eventName || item.fun !== fun || item.target != target; });
    }
    emit(eventName, ...args) {
        let currentEvents = this.events.filter(item => { return item.eventName == eventName });
        currentEvents.map(item => {
            item.fun.apply(item.target, args);
        });
        this.events = this.events.filter(item => { return item.eventName != eventName || (item.eventName == eventName && !item.isOnce) });
        return currentEvents
    }
    removeAllListeners(eventName) {
        if(typeof(eventName) == 'object' || typeof eventName == 'function'){
            this.events = this.events.filter(item => { return item.target != eventName });
        } else {
            this.events = this.events.filter(item => { return item.eventName != eventName });
        }
    }
    once(eventName, fun, target) {
        this.events.push({ eventName: eventName, fun: fun, target: target, isOnce: true });
    }
    clean(){
        this.events = [];
    }
}