import {Actor} from "./Actor";
import seedrandom from "seedrandom"
import {Frame} from "./Frame";
export class Engine extends Actor{

    random:any = null

    frames:Array<Frame> = []
    currentFrame : number = 0

    constructor() {
        super();
        this.runScene = this
    }

    initSeed(seed:string){
        if(this.random == null){
            this.random = new seedrandom(seed)
        } else {
            throw new Error("This seed can only be called once")
        }
    }

    resetFrames(){
        this.currentFrame = 0
        this.frames = []
    }

    getRandom(max:number){
        return this.random() * max
    }

    onFrameMsg(frame:number,data:Uint8Array){
        if(!!this.frames[frame]){
            console.log("重复帧:",frame)
        } else {
            this.currentFrame = Math.max(frame,this.currentFrame)
            this.frames[frame] = new Frame(data)
        }
    }
}