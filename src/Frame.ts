export class Frame {
    private data : Uint8Array
    constructor(data:Uint8Array) {
        this.data = data
    }

    get():any{
        return this.data
    }

}