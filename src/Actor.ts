import {Vec3} from "./Vec3";
import Event from "./Event"
import {Engine} from "./Engine";
import {Component} from "./Component";
import {EventType} from "./EventType";

type _types_globals__Constructor<T = unknown> = new (...args: any[]) => T;

export class Actor extends Event {
    _name  = ""
    _uuid : string = ""
    _valid : boolean = true

    get point(){
        return this._point
    }
    set point(v:Vec3){
        var p = this._point.clone()
        this._point = v
        this.emit(EventType.Actor.POSITION_CHANGE,this,p,v)
    }
    private _point:Vec3 = Vec3.ZERO;

    get rotate(){
        return this._rotate
    }
    set rotate(v:Vec3){
        var p = this._rotate.clone()
        this._rotate = v
        this.emit(EventType.Actor.ANGULAR_CHANGE,this,p,v)
    }
    private _rotate:Vec3 = Vec3.ZERO;

    get enabled(){
        return this._enabled
    }
    set enabled(v){
        if(this._enabled != v){
            this._enabled = v
            if(v){
                this.emit(EventType.Actor.ON_ENABLED)
            } else {
                this.emit(EventType.Actor.OFF_ENABLED)
            }
        }
    }
    private _enabled : boolean = true

    childrens:Array<Actor> = []
    components:Array<Component> = []

    parent : Actor = null
    runScene:Engine = null

    constructor(name?:string) {
        super()
        this._name = name || "ator"
    }

    addChild(actor:Actor){
        if (actor.parent != null){
            console.log("重复添加节点:",actor.parent)
            return
        }
        actor.parent = this
        actor.runScene = this.runScene
        actor._uuid = ""+this.runScene.getRandom(8)
        actor._name = actor.constructor.name
        this.childrens.push(actor)
    }

    removeChild(actor:Actor){
        for(var i=0; i<this.childrens.length; i++){
            var o = this.childrens[i]
            if(o == actor){
                this.childrens.splice(i,1)
                o._valid = false
                o.parent = null
                this.emit(EventType.Actor.DESTROYED,this)
                return true
            }
        }
        return false
    }

    isValid(){
        return this._valid
    }

    destroy(){
        if(!!this.parent){
            this.parent.removeChild(this)
        }
    }


    addComponent<T extends Component>(T):T{
        // @ts-ignore
        var com = new T()
        com._uuid = ""+this.runScene.getRandom(8)
        com._name = com.constructor.name
        com.actor = this
        this.components.push(com)
        com.Awake()
        com.OnEnable()
        return com
    }

    getComponent(className: typeof Component | string): Component | null{
        var isStr = typeof  className == "string"
        for(var i=0; i<this.components.length; i++){
            var o = this.components[i]
            // @ts-ignore
            if(isStr ? className == o._name : o instanceof className){
                return this.components[i]
            }
        }
        return null
    }

    destroyComponent(com:Component){
        for(var i=0; i<this.components.length; i++){
            var o = this.components[i]
            // @ts-ignore
            if(o instanceof com){
                o.OnDestroy()
                this.components.splice(i,1)
            }
        }
    }


}

