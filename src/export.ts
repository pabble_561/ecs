import {Engine} from "./Engine";
import {Actor} from "./Actor";
import {Vec3} from "./Vec3";
import {Component} from "./Component";
import {EventType} from "./EventType";
import Event from "./Event";
import {Move} from "./Move";
import Decimal = require('../libs/decimal.js');

var ecs = {
    Engine,
    Actor,
    Vec3,
    Component,
    Event,
    EventType,
    Move
}

export default ecs

globalThis.ecs = ecs
globalThis.Decimal = Decimal