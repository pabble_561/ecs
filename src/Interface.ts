export interface Frame {
    onFrame(frame:number,data:Uint8Array)
}

export interface LifeCycle {
    Awake();
    OnEnable();
    OnDisabled();
    OnDestroy()
}