import Decimal = require('../libs/decimal.js');

export class Vec3 {

    x = new Decimal(0)
    y = new Decimal(0)
    z = new Decimal(0)

    // @ts-ignore
    constructor(_x:Decimal|Vec3,_y?:Decimal,_z?:Decimal){
        if (_x instanceof Vec3){
            this.x = _x.x
            this.y = _x.y
            this.z = _x.y
        } else {
            this.x = _x
            this.y = _y
            this.z = _z
        }
    }
    static get ZERO(){
        return Vec3.from(0,0,0)
    }

    static from(_x:number,_y:number,_z:number){
        return new Vec3(new Decimal(_x),new Decimal(_y),new Decimal(_z))
    }

    clone(){
        return Vec3.from(this.x.toNumber(),this.y.toNumber(),this.z.toNumber())
    }

    add(_x:Decimal|Vec3,_y?:Decimal,_z?:Decimal){
        var o = this.clone()
        if (_x instanceof Vec3){
            o.x = o.x.add(_x.x)
            o.y = o.y.add(_x.y)
            o.z = o.z.add(_x.z)
        } else {
            o.x = o.x.add(_x)
            o.y = o.y.add(_y)
            o.z = o.z.add(_z)
        }
        return o
    }

    sub(_x:Decimal|Vec3,_y?:Decimal,_z?:Decimal){
        var o = this.clone()
        if (_x instanceof Vec3){
            o.x = o.x.sub(_x.x)
            o.y = o.y.sub(_x.y)
            o.z = o.z.sub(_x.z)
        } else {
            o.x = o.x.sub(_x)
            o.y = o.y.sub(_y)
            o.z = o.z.sub(_z)
        }
        return o
    }

    mul(_x:Decimal|Vec3,_y?:Decimal,_z?:Decimal){
        var o = this.clone()
        if (_x instanceof Vec3){
            o.x = o.x.mul(_x.x)
            o.y = o.y.mul(_x.y)
            o.z = o.z.mul(_x.z)
        } else {
            o.x = o.x.mul(_x)
            o.y = o.y.mul(_y)
            o.z = o.z.mul(_z)
        }
        return o
    }

    div(_x:Decimal|Vec3,_y?:Decimal,_z?:Decimal){
        var o = this.clone()
        if (_x instanceof Vec3){
            o.x = o.x.div(_x.x)
            o.y = o.y.div(_x.y)
            o.z = o.z.div(_x.z)
        } else {
            o.x = o.x.div(_x)
            o.y = o.y.div(_y)
            o.z = o.z.div(_z)
        }
        return o
    }



}